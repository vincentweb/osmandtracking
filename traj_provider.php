<?php
$traj = file('traj.txt');
// $traj = array_slice($traj, 0, 553);
$last = json_decode(end($traj), true);

$positions = array();
foreach ($traj as &$spos) {
    $pos = json_decode($spos, true);
    array_push($positions, [$pos["lat"], $pos["lon"], $pos["speed"]*3.6186]);
}

$traj_info = array("last" => $last, "positions" => $positions);

header('Content-Type: application/json');
echo json_encode($traj_info, JSON_PRETTY_PRINT);
?>