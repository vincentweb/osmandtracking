<?php
$handle = fopen("traj.txt","a+");
if (!$handle)
    die("Cannot access file.");

if(flock($handle, LOCK_EX)) {
    fwrite($handle, json_encode($_GET)."\n");
    flock($handle, LOCK_UN);
} else {
    die("Could not Lock File!");
}

fclose($handle);
?>
