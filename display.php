<html>
<head>
 <link rel="stylesheet" href="https://unpkg.com/leaflet@1.2.0/dist/leaflet.css"
   integrity="sha512-M2wvCLH6DSRazYeZRIm1JnYyh22purTM+FDB5CsyxtQJYeKq83arPe5wgbNmcFXGqiSH2XR8dT/fJISVA1r/zQ=="
   crossorigin=""/>
 <script src="https://unpkg.com/leaflet@1.2.0/dist/leaflet.js"
   integrity="sha512-lInM/apFSqyy1o6s89K4iQUKg6ppXEgsVxT35HbzUupEVRh2Eu9Wdl4tHj7dZO0s1uvplcYGmt3498TtHq+log=="
   crossorigin=""></script>
<script src='https://api.mapbox.com/mapbox.js/v3.3.1/mapbox.js'></script>
<link href='https://api.mapbox.com/mapbox.js/v3.3.1/mapbox.css' rel='stylesheet' />
<script src="leaflet.hotline.js"></script>
</head>
<body>
<div id="mapid" style="height: 95%"></div>
<script>
// https://github.com/bbecquet/Leaflet.PolylineDecorator
// https://github.com/iosphere/Leaflet.hotline
// Other colormaps: time, elevation
var marker = null;
var polyline = null;
//mapboxgl.accessToken = 'pk.eyJ1IjoidmluY2VudHdlYiIsImEiOiJjajZreWI5N3cxZmV5MnFueWFoMDJ0ZWFhIn0.--f4cGegw7Ey7vLBuW8mGg';
//var mymap = new mapboxgl.Map({
//  container: 'mapid',
//  center: [1.537887, 43.677376],
//  zoom: 15,
//  style: 'mapbox://styles/mapbox/streets-v11'
//});
//mymap.addControl(new mapboxgl.NavigationControl());

L.mapbox.accessToken = 'pk.eyJ1IjoidmluY2VudHdlYiIsImEiOiJjajZreWI5N3cxZmV5MnFueWFoMDJ0ZWFhIn0.--f4cGegw7Ey7vLBuW8mGg';
var mapboxTiles = L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/streets-v11/tiles/{z}/{x}/{y}?access_token=' + L.mapbox.accessToken, {
       attribution: '© <a href="https://www.mapbox.com/feedback/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
       tileSize: 512,
       zoomOffset: -1
});
var mymap = L.map('mapid')
  .addLayer(mapboxTiles)
  .setView([43.677376, 1.537887], 15);


//var mymap = L.map('mapid').setView([43.677376, 1.537887], 15);
//L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
//    maxZoom: 20,
//    id: 'mapbox.streets',
//    accessToken: 'pk.eyJ1IjoidmluY2VudHdlYiIsImEiOiJjajZreWI5N3cxZmV5MnFueWFoMDJ0ZWFhIn0.--f4cGegw7Ey7vLBuW8mGg'
//}).addTo(mymap);

var request = new XMLHttpRequest();

request.onload = function() {
  if (request.status >= 200 && request.status < 400) {
    var traj_data = JSON.parse(request.responseText);
    // console.debug("Found "+traj_data.positions.length);
    if (traj_data.positions.length > 0) {
      
      if (marker == null) {
        mymap.setView([traj_data.last.lat, traj_data.last.lon], 15);
        marker = L.marker([traj_data.last.lat, traj_data.last.lon]).addTo(mymap);
        polyline = L.hotline(traj_data.positions, 
          {
              // color: 'red',
              weight: 5,
              min: 0,
              max: 60,
              palette: { 0.0: 'red', 0.33: 'yellow', 0.66: 'green' }, // yellow=20km, green=40km
              // opacity: .7,
          }
          ).addTo(mymap);
      }
      else {
        marker.setLatLng([traj_data.last.lat, traj_data.last.lon]);
        polyline.setLatLngs(traj_data.positions);
      } 
    }
  } else {
    console.warn("Failed to load traj_data");
  }
};
request.onerror = function() {
  console.warn("Error while loading traj_data");
};

function test() {
  request.open('GET', "traj_provider.php", true);
  request.send();
}

window.setInterval(test, 1000);
</script>
</body>
</html>
