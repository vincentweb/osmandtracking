Store and display live trajectory information sent by OsmAnd

Similar project:
https://github.com/megarocks/osmand_tracker
https://github.com/alienff/osmm

Projects to display GPX files:
https://github.com/janvonde/pregos-tracks
https://github.com/valentinbonneaud/mapProject
https://github.com/jdesgats/opengpxmapper

